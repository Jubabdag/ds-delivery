import './styles.css';
import { ReactComponent as YoutubeIcon } from './youtube.svg';
import { ReactComponent as LinkidIcon } from './linkedin.svg';
import { ReactComponent as InstagramIcon } from './instagram.svg';

function Footer(){
    return (
        <footer className="main-footer">
            App desenvolvido durante a 2ª edição do evento semana DevSuperior
            <div className="footer-icons">
                <a href="https://www.youtube.com/user/ESGURMITUU" target="_new">
                    <YoutubeIcon/>
                </a>
                <a href="https://instagram.com/wiliam.depiro" target="_new">
                    <InstagramIcon/>
                </a>
                <a href="https://www.linkedin.com/in/wiliammd/" target="_new">
                    <LinkidIcon/>
                </a>

            </div>
        </footer>
    )
}
export default Footer;